variable "prefix" {
  default = "<environment_prefix>"
}

variable "project" {
  default = "<gcp_project_id>"
}

variable "region" {
  default = "<gcp_region>"
}

variable "zone" {
  default = "<gcp_zone>"
}

variable "external_ip" {
  default = "<external_ip>"
}

variable "geo_site" {
  default = "<geo_site>"
}

variable "geo_deployment" {
  default = "<geo_deployment>"
}
